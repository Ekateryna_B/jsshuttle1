> # Twitter API
>
> ## Document contents:
>
> 1. [Introduction, description](#intro)
> 2. [API overview](#over)
> 2. [Authorization, getting access](#auth)
> 3. [Common JSON api](#json)
> 4. [Streaming api](#streaming)

<br>
<br>
<a name="intro"></a>
## Introduction, description

[**Twitter**](https:\\twitter.com) as any other popular service has it's own API for programmatic access to available data in order to provide best exprience for developers interested in it. For the last years as **Twiter** groving their API also developed a lot. Now common api has 98 methods which provide read-write access to almost any possible entity in **Twitter**, such as tweets, subscriptions, timelines, statuses, followers, users, blocked, e.t.c. Yet like a number of [solutions](https://dev.twitter.com/solutions) for web and mobile apps such as: [Fabric](https://get.fabric.io/), [Crashlitycs](https://fabric.io/kits/android/crashlytics), [Digits](https://www.digits.com/), [Monetize](https://fabric.io/kits/android/mopub), [Twitter kit](https://fabric.io/kits/android/twitterkit), [Twitter for web](https://dev.twitter.com/web/overview), [Twitter Cards](https://dev.twitter.com/cards/overview), [Answers](https://answers.io/).

All required docs and materials are located on their [**Twitter Development Portal**](https://dev.twitter.com/). Also **Twitter** development team always open for a feedback and requests on their [feedback forum](https://twittercommunity.com/c/rest-api/rest-api-v1-1). Current stable version of rest API is 1.1. You may wan't to see [upcoming changes overview](https://dev.twitter.com/overview/api/upcoming-changes-to-tweets) to prepare yourself for migrating when new  version will be releaced.

Also **Twitter** provides two simple and useful tools for developers:

* [Console for testing API](https://dev.twitter.com/rest/tools/console)
* [Api status overview](https://dev.twitter.com/overview/status)

#### Twitter policies

* [Twitter developer policy](https://dev.twitter.com/overview/terms/policy)
* [Twitter developer agreement](https://dev.twitter.com/overview/terms/agreement)
* [Display requirements](https://about.twitter.com/company/display-requirements)
* [Geo guildelines](https://dev.twitter.com/overview/terms/geo-developer-guidelines)

<br>
<br>
<a name="over"></a>
## Api overview

There are four main “objects” that you’ll encounter in the API: [Tweets](#tweet), [Users](#user), [Entities](#ent) (see also [Entities in Objects](#ent2)), and [Places](#places). See the anatomy of these objects, and learn about properties like [Twitter IDs](https://dev.twitter.com/overview/api/twitter-ids-json-and-snowflake) or [Place Attributes](https://dev.twitter.com/overview/api/places#attributes) to know what to expect.
Check out best practices around [Connecting to Twitter API using SSL](https://dev.twitter.com/overview/api/ssl), [sing cursors to navigate collections](https://dev.twitter.com/overview/api/cursoring) and [Error Codes &amp; Responses](https://dev.twitter.com/overview/api/response-codes) to learn how to most effectively interact with APIs.

Also twitter has a large [number of libraries](https://dev.twitter.com/overview/api/twitter-libraries) available for all popular programming languages, we interested in this ones:

* [Node twiter](github.com/desmondmorris/node-twitter) @desmondmorris - An asynchronous client library for the Twitter REST and Streaming API's. <span style="color:#990000">**Simplest and the best as for me - STRONGLY suggest to use**</span>
* [cordovaTwitter](https://github.com/jmtt89/cordovaTwitter) @jmtt89 - Twitter client plugin for Cordove apps.
* [witterJSClientby](https://github.com/BoyCook/TwitterJSClient) @BoyCook — Twitter client library written in Javascript and packaged as a node module
* [user-streamby](https://github.com/aivis/user-stream) @AivisSilins — a simple Node.js User streams client

<a name="user"></a>
### [Object: Users](https://dev.twitter.com/overview/api/users)
Users can be anyone or anything. They tweet, follow, create lists, have a home_timeline, can be mentioned, and can be looked up in bulk.

![User](https://g.twimg.com/dev/documentation/image/user-web-intent.png)

Consumers of Users should tolerate the addition of new fields and variance in ordering of fields with ease. Not all fields appear in all contexts. It is generally safe to consider a nulled field, an empty set, and the absence of a field as the same thing.

<a name="tweet"></a>
### [Object: Tweets](https://dev.twitter.com/overview/api/tweets)
Tweets are the basic atomic building block of all things Twitter. Tweets, also known more generically as “status updates.” Tweets can be embedded, replied to, liked, unliked and deleted.

![Tweet](https://g.twimg.com/TwitterAPI_tweet.png)

Consumers of Tweets should tolerate the addition of new fields and variance in ordering of fields with ease. Not all fields appear in all contexts. It is generally safe to consider a nulled field, an empty set, and the absence of a field as the same thing. Please note that Tweets found in Search results vary somewhat in structure from this document.

<a name="ent"></a>
### [Object: Entities](https://dev.twitter.com/overview/api/entities)
* [Hashtags](https://dev.twitter.com/overview/api/entities#obj-hashtags)
* [Media](https://dev.twitter.com/overview/api/entities#obj-media)
* [Size](https://dev.twitter.com/overview/api/entities#obj-size)
* [Sizes](https://dev.twitter.com/overview/api/entities#obj-sizes)
* [Url](https://dev.twitter.com/overview/api/entities#obj-url)
* [User mention](https://dev.twitter.com/overview/api/entities#obj-usermention)

Entities provide metadata and additional contextual information about content posted on Twitter. Entities are never divorced from the content they describe. In API v1.1, entities are returned wherever Tweets are found in the API. Entities are instrumental in resolving URLs.

Read Entities in Objects for a more comprehensive guide to how entities are used throughout Twitter objects.

Consumers of Entities should tolerate the addition of new fields and variance in ordering of fields with ease. Not all fields appear in all contexts. It is generally safe to consider a nulled field, an empty set, and the absence of a field as the same thing.

<a name="ent2"></a>
### [Object: Entities in Objects](https://dev.twitter.com/overview/api/entities-in-twitter-objects)
Entities are intimately related to other Twitter Objects. Listed below are the various relationships that Entities have, and any additional attributes needed for that relationship.

* [Entities for Tweets](https://dev.twitter.com/overview/api/entities-in-twitter-objects#tweets)
  * [media](https://dev.twitter.com/overview/api/entities-in-twitter-objects#media)
  * [urls](https://dev.twitter.com/overview/api/entities-in-twitter-objects#urls)
  * [user_mentions](https://dev.twitter.com/overview/api/entities-in-twitter-objects#user_mentions)
  * [hashtags](https://dev.twitter.com/overview/api/entities-in-twitter-objects#hashtags)
  * [symbols](https://dev.twitter.com/overview/api/entities-in-twitter-objects#symbols)
  * [extended_entities](https://dev.twitter.com/overview/api/entities-in-twitter-objects#extended_entities)
* [Entities for Retweets](https://dev.twitter.com/overview/api/entities-in-twitter-objects#retweets)
* [Entities for Users](https://dev.twitter.com/overview/api/entities-in-twitter-objects#users)
* [Entities for Direct Messages](https://dev.twitter.com/overview/api/entities-in-twitter-objects#dms)

<a name="places"></a>
### [Object: Places](https://dev.twitter.com/overview/api/places)
Places are specific, named locations with corresponding geo coordinates. They can be attached to Tweets by specifying a place_id when tweeting. Tweets associated with places are not necessarily issued from that location but could also potentially be about that location. Places can be searched for. Tweets can also be found by place_id.

Places also have an attributes field that further describes a Place. These attributes are more convention rather than standard practice, and reflect information captured in the Twitter places database. See Place Attributes for more information.
 * [Places field guide](https://dev.twitter.com/overview/api/places#field_guide)
 * [Places Attributes](https://dev.twitter.com/overview/api/places#attributes)

<br>
<br>
<a name="auth"></a>
## Authorization, getting access

Twitter using [**Oauth** 1.0a](http://oauth.net/) to provide authorized access to its API. There are two main forms of authorization:

**Application-user authentication**<br>
This is the most common form of resource authentication in Twitter’s OAuth 1.0A implementation to date. Your signed request both identifies your application’s identity in addition to the identity accompanying granted permissions of the end-user you’re making API calls on behalf of, represented by the user’s access token.

[**Application-only authentication**](https://dev.twitter.com/oauth/application-only)<br>
Application-only authentication is a form of authentication where your application makes API requests on its own behalf, without a user context. API calls are still rate limited per API method, but the pool each method draws from belongs to your entire application at large, rather than from a per-user limit. API methods that support this form of authentication will contain two rate limits in their documentation, one that is per user (for application-user authentication) and the other is per app (for this form of application-only authentication). Not all API methods support application-only authentication.

The application-only auth flow follows these steps:
* An application encodes its consumer key and secret into a specially encoded set of credentials.
* An application makes a request to the POST oauth2 / token endpoint to exchange these credentials for a bearer token.
* When accessing the REST API, the application uses the bearer token to authenticate.

Because there is no need to sign a request, this approach is dramatically simpler than the standard OAuth 1.0a model.

![Oauth flow](https://g.twimg.com/dev/documentation/image/appauth_0.png)

#### Register an app
You'll need to register an application with **Twitter** to obtain your first [OAuth](http://oauth.net/) application keys. Visit the [Twitter Developer](https://dev.twitter.com) site and click [Manage Your Apps](https://apps.twitter.com/). Click **Create New Apps**.

> *Note you need to have default Twitter account you can [create it here](https://twitter.com/signup) and this account should have a verificated mobile phone number you can add it [here](https://twitter.com/settings/add_phone)*

This will lead you to simple and straightforward form. Where you point your APP name, description, e.t.c
But we should note **Callback URL** field this is location where Oauth should redirect after successfully authenticating. After that you will be redirected to App settings page where you'll need to configure the app permissions and make notes of the application OAuth key and secret, and create access token. Also you may click **Test Oauth** button in top right corner to send test calls right away.

And [Here Twitter provides descriptions](https://dev.twitter.com/oauth/overview) for different kinds of authentification and corresponds types to purposes: "If i need to <% taskname %> then i should use <% this %>"



#### Authentification summary

1. [Create twitter account](https://twitter.com/signup) and [verify your phone](https://twitter.com/settings/add_phone).
2. Go to [Twitter Apps Management](https://apps.twitter.com/) and **Create new App**
3. Fill the form and click **Create**
4. Switch to **Keys and Access Tokens** tab and click **Generate My Access Token**
5. Modify permissions for app if that is needed.
6. Try not lost access to your app page. Because Consumer Key, Consumer Secret, Access Token, Access Token Secret and Owner ID required to set up and authorize our Node.js App.
7. We also will require **Bearer token** [Here is described how to get it](https://dev.twitter.com/oauth/application-only). In general through<br>`POST https://api.twitter.com/oauth2/token` call.
8. <span style="color:#990000">**If you got something like this. Nicely done! You are a pro developer**</span> ![Twitter App Page](http://i.imgur.com/5g61FtK.png)

<br>
<br>
<a name="json"></a>
## [Common JSON API](https://dev.twitter.com/rest/public)
The REST APIs provide programmatic access to read and write Twitter data. Author a new Tweet, read author profile and follower data, and more. The REST API identifies Twitter applications and users using OAuth; responses are available in JSON.

If your intention is to monitor or process Tweets in real-time, consider using the [Streaming API](#streaming) instead.

For an example, consider a web application which accepts user requests, makes one or more requests to Twitter’s API, then formats and prints the result to the user, as a response to the user’s initial request

![REST](https://g.twimg.com/dev/documentation/image/streaming-intro-1_1.png)

#### Default entities and retweets
Where applicable, [entities](https://dev.twitter.com/overview/api/#ent2) and retweets are returned by default. Entities are returned as part of Tweet objects unless the `include_entities` parameter is set to `false`. Native retweets are included in timelines unless the `include_rts parameter` is set to `false`.

#### Authentication on all endpoints
We require applications to authenticate all of their requests with [**OAuth 1.0a**](https://dev.twitter.com/oauth) or [Application-only authentication](https://dev.twitter.com/oauth/application-only). This visibility allows us to prevent abusive behavior, and it also helps us to further understand how categories of applications are using the API. We apply this understanding to better meet the needs of developers as we continue to evolve the platform.

##### Rate Limiting
We divide the rate limit window into 15 minute chunks per endpoint, with most individual calls allowing for 15 requests in each window. This is particularly advantageous for apps making calls such as `GET statuses/show/:id`, `GET users/lookup`, `GET search/tweets` and others. Be sure to read the [API Rate Limiting documentation](https://dev.twitter.com/rest/public/rate-limiting) as well as review the per method limits available here. Use this `GET application/rate_limit_status ` to get info about your limits.

#### Twitter client policies
All applications replicating the core Twitter experience, usually called “clients”, must adhere to certain restrictions, including a 100,000 user token limit. **To be clear, the 100,000 user token limit applies only to the small number of clients replicating the core Twitter experience - it does not apply to the majority of other applications in the broader ecosystem**. These clauses are outlined in detail in the [Developer Rules of the Road](https://dev.twitter.com/terms/api-terms).

#### REST API can be devided on this groups:
* statuses management
* followers management
* friendship management
* Twitter help
* Geolocation and
* Users
* trends
* mutes
* account management
* searches
* lists
* rate limit checkout
* blocks management
* direct messages

#### Working with Timelines

[**Timeline documentation**](https://dev.twitter.com/rest/public/timelines)<br>
The Twitter API has several methods, such as `GET statuses/user_timeline`, `GET statuses/home_timeline` and `GET search/tweets`, which return a timeline of Tweet data. Such timelines can grow very large, so there are limits to how much of a timeline a client application may fetch in a single request. Applications must therefore iterate through timeline results in order to build a more complete list.

Because of Twitter’s realtime nature and the volume of data which is constantly being added to timelines, standard paging approaches are not always effective. The goal of this page is to demonstrate the issues Twitter developers may face when paging through result sets and to give best practices for processing a timeline.

#### REST api usage summary
* Open [REST APIs](https://dev.twitter.com/rest/public) page where all the docs are located
* Open [API Console tool](https://dev.twitter.com/rest/tools/console) to configure and debug your calls right there
* Rememer to look [Rate Limits Chart](https://dev.twitter.com/rest/public/rate-limits). To avoid surprices
* Open [Responce codes spreadsheet](https://dev.twitter.com/overview/api/response-codes) this will help you to understand what **Twitter** trying to tell you
* If you wan't detailed info about Twitter entities [look here](#over). All properties [described and has examples here](https://dev.twitter.com/overview/api).
* [Timelines](https://dev.twitter.com/rest/public/timelines) one of main features of **Twitter** use it wisely.
* This is how your tabbar should look like<br>![Tabbar](http://i.imgur.com/QNkknZ4.png)
* Most likely [node-twitter](https://github.com/desmondmorris/node-twitter) module for node.js will be used to handle calls. And our code will look something like this:<br>
````javascript
var client = new Twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  bearer_token: process.env.TWITTER_BEARER_TOKEN,
});

client.get(path, params, callback);
client.post(path, params, callback);
client.stream(path, params, callback);

// Get favorites list
client.get('favorites/list', function(error, tweets, response) {
  if(error) throw error;
  console.log(tweets);  // The favorites.
  console.log(response);  // Raw response object.
});

// Tweet something
client.post('statuses/update', {status: 'I Love Twitter'},  function(error, tweet, response) {
  if(error) throw error;
  console.log(tweet);  // Tweet body.
  console.log(response);  // Raw response object.
});

// Or connect to live stream
var stream = client.stream('statuses/filter', {track: 'javascript'});
stream.on('data', function(tweet) {
  console.log(tweet.text);
});

stream.on('error', function(error) {
  throw error;
});

// You can also get the stream in a callback if you prefer.
client.stream('statuses/filter', {track: 'javascript'}, function(stream) {
  stream.on('data', function(tweet) {
    console.log(tweet.text);
  });

  stream.on('error', function(error) {
    throw error;
  });
});
````

<br>
<br>
<a name="streaming"></a>
## [Streaming API](https://dev.twitter.com/streaming/overview)
The Streaming APIs give developers low latency access to Twitter’s global stream of Tweet data. A proper implementation of a streaming client will be pushed messages indicating Tweets and other events have occurred, without any of the overhead associated with polling a REST endpoint.

If your intention is to conduct singular searches, read user profile information, or post Tweets, consider using the [REST APIs](#json) instead.

Twitter offers several streaming endpoints, each customized to certain use cases.

* **[Public streams](https://dev.twitter.com/streaming/public)** Streams of the public data flowing through Twitter. Suitable for following specific users or topics, and data mining.
* **[User streams](https://dev.twitter.com/streaming/userstreams)** Single-user streams, containing roughly all of the data corresponding with a single user’s view of Twitter.
* **[Site streams](https://dev.twitter.com/streaming/sitestreams)** The multi-user version of user streams. Site streams are intended for servers which must connect to Twitter on behalf of many users.
* [Firehose](https://dev.twitter.com/streaming/reference/get/statuses/firehose) `GET statuses/firehose` - ineresting stream to retrieve all public statuses

#### Connecting to a streaming endpoint
Establishing a connection to the streaming APIs means making a very long lived HTTP request, and parsing the response incrementally. Conceptually, you can think of it as downloading an infinitely long file over HTTP. This is called "long-pooling request". Authentification is the same way as for [REST API](#json) is Oauth 1.0a.

#### Connecting
To connect to the Streaming API, form a HTTP request and consume the resulting stream for as long as is practical. Our servers will hold the connection open indefinitely, barring server-side error, excessive client-side lag, network hiccups, routine server maintenance or duplicate logins.

The method to form an HTTP request and parse the response will be different for every language or framework, so consult the documentation for the HTTP library you are using.

Some HTTP client libraries only return the response body after the connection has been closed by the server. These clients will not work for accessing the Streaming API. You must use an HTTP client that will return response data incrementally. Most robust HTTP client libraries will provide this functionality.

Connecting to the streaming API requires keeping a persistent HTTP connection open. In many cases this involves thinking about your application differently than if you were interacting with the REST API.

An app which connects to the Streaming APIs will not be able to establish a connection in response to a user request, as shown in the above example. Instead, the code for maintaining the Streaming connection is typically run in a process separate from the process which handles HTTP requests

![Streaming api](https://g.twimg.com/dev/sites/default/files/images_documentation/streaming-intro-2_1.png)

The streaming process gets the input Tweets and performs any parsing, filtering, and/or aggregation needed before storing the result to a data store. The HTTP handling process queries the data store for results in response to user requests. While this model is more complex than the first example, the benefits from having a realtime stream of Tweet data make the integration worthwhile for many types of apps.

#### Rate limiting
Clients which do not implement backoff and attempt to reconnect as often as possible will have their connections rate limited for a small number of minutes. Rate limited clients will receive HTTP 420 responses for all connection requests.

Clients which break a connection and then reconnect frequently (to change query parameters, for example) run the risk of being rate limited.

Twitter does not make public the number of connection attempts which will cause a rate limiting to occur, but there is some tolerance for testing and development. A few dozen connection attempts from time to time will not trigger a limit. However, it is essential to stop further connection attempts for a few minutes if a HTTP 420 response is received. If your client is rate limited frequently, it is possible that your IP will be blocked from accessing Twitter for an indeterminate period of time.

#### Streaming summary
* [How to connect](https://dev.twitter.com/streaming/overview/connecting)
* [How to customize it](https://dev.twitter.com/streaming/overview/request-parameters)
* [Streaming message types](https://dev.twitter.com/streaming/overview/messages-types)
* [Process stream data](https://dev.twitter.com/streaming/overview/processing)
* Again [node-twitter](https://github.com/desmondmorris/node-twitter) there we have all required docs and examples